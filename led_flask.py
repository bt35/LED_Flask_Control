#import time, pytz 
from flask import Flask, flash, redirect, render_template, request, session, abort, jsonify
#from datetime import datetime
from bibliopixel.led import *
from bibliopixel.animation import StripChannelTest
from bibliopixel.drivers.LPD8806 import *
from bibliopixel import LEDStrip
import bibliopixel.colors as colors

# Some of the above imports are not needed, but will later be used to control LED animations
# and maybe some date time, weather type stuff. 

app = Flask(__name__)

# LED initialization for biblipixel
# Set default size of LED
numLeds = 240 
driver = DriverLPD8806(numLeds, c_order = ChannelOrder.BRG)
led = LEDStrip(driver)
# Start out with the LED set to off.
r, g, b = (0,0,0)
led.fillRGB(r,g,b)

# A function to set the color of the LED strip... 
# The higher the slider the more bright a color will be. 
# Global vairables for color are used in a non practical way to 
# ensure that all methods have the most up to date colors values. 
def set_color():
    global r, g, b
    led.fillRGB(r,g,b)
    led.update()

# Not enabled yet.. I'm not sure how to 
# dynamically set the driver's number of leds. 
#@app.route('/enable_leds',  methods=['POST'] )
#def enable_led():
#    global num_leds
#    print (request.form['enable'])
#    num_leds = int(request.form['enable'])
#    set_color()
#    return "LED Function Not Enabled Yet"

# Handle the /red ajax slider. 
@app.route('/red',  methods=['POST'] )
def red():
    global r
    print (request.form['red'])
    r = int(request.form['red'])
    set_color()
    return str(r)

# Handle the /green ajax slider. 
@app.route('/green',  methods=['POST'] )
def green():
    global g
    print (request.form['green'])
    g = int(request.form['green'])
    set_color()
    return str(g) #render_template('index.html', r=str(g))

# Handle the blue ajax slider. 
@app.route('/blue',  methods=['POST'] )
def blue():
    global b
    print (request.form['blue'])
    b = int(request.form['blue'])
    set_color()
    return str(b) #render_template('index.html', r=str(b))

@app.route('/Off')
def off():
    led.fillRGB(0,0,0)
    led.update()
    return "Nothing" # Return something.


# Routes
# Route to / and show the index hmtl. 
@app.route("/")
def index():
    global r,g,b
    return render_template('index.html' , r = str(r), g = str(g), b = str(b))



if __name__ == "__main__":
        app.run(host= '0.0.0.0')



