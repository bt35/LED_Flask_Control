## What
A flask application to control an LDP8806 LED strip using sliders, usning python2 version of biblio pixel. 

## How
This application uses python2, flask (jinja and others), and BiblioPixel.

The LED strip can be found here: https://www.adafruit.com/product/306?length=1. Follow Adafruit's power guidance. 

The LED data connections are the same as seen here:  

```
Please follow the guide from Adafruit here for powering a 1M LED strip: https://learn.adafruit.com/digital-led-strip
In this project I use a DC jack to adapt the plug from a surge protector to the LED strip. The positive (power side) of the adapter is connected to the red cable of the LED strip and the negative (ground) side of the adapter is attached to a ground port on the AutomationHat.

Black, connected via a gray jumper to the ground. This means the LED strip itself is grouneded. The other ground above is for the power adapter.
Green, connected via a white jumper to he SCLK pin.
Yellow, connected via a blue jumper to the MOSI pin.
```

## TODO:
Update to biblioPixel3. This will get done.. once I figure out how to use pythong virtual environments to package an application. 

### Gif...
![alt_text](/thermostat_pi_automation_hat/output.gif)

